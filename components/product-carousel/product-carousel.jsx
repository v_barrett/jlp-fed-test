import React, { useState } from 'react';
import styles from "./product-carousel.module.scss";

const ProductCarousel = ({ images }) => {
  const [currentImage, setCurrentImage] = useState(images[0])

  const handleClick = () => {
    setCurrentImage((prevImage) => {
      const prevImageIndex = images.indexOf(prevImage)

      return prevImageIndex === (images.length - 1) ? images[0] : images[prevImageIndex + 1]
    })
  }

  if (images.length > 1) {
    return (
      <div className={styles.productCarousel}>
        <button onClick={handleClick}>Next</button>
        <img src={currentImage} alt="" style={{ width: "100%", maxWidth: "500px" }} />
      </div>
    )
  } else {
    return (
      <img src={images[0]} alt="" style={{ width: "100%", maxWidth: "500px" }} />
    )
  }
};

export default ProductCarousel;
