import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import ProductCarousel from './product-carousel'
import { act } from 'react-dom/test-utils'

describe('ProductCarousel', () => {
  const images = [
    '/image/1',
    '/image/2',
    '/image/5',
    '/image/3',
    '/image/4'
  ]

  it('displays the first image on page load', () => {
    render(<ProductCarousel images={images} />)

    const image = screen.getByRole('img')

    expect(image.src).toContain('/image/1')
  })

  it('shows the next image on Next button click', () => {
    render(<ProductCarousel images={images} />)

    const button = screen.getByRole('button')
    const image = screen.getByRole('img')

    expect(image.src).toContain('/image/1')

    act(() => {
      button.click()
    })

    expect(image.src).toContain('/image/2')
  })
})
