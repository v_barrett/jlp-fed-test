# UI Dev Technical Test - Dishwasher App

## Notes
Unfortunately I was only able to implement the most basic functionality and design within the recommended time (3 hours) without having much time to consider accessibility, performance, tooling and workflow. My notes below document my assumptions and any improvements I would have included had I had more time on this project.

### Assumptions

- API access: Calls to the API seem to rely on a particular user agent. Curling the API returned an ‘access denied’ error. Calling the API via the application hung without returning anything. The API returned results correctly in my Chrome browser. I’ve made the assumption that this is a result of some unintentional locking down of the API, and have therefore set the user agent headers in the code to a Chrome browser. If I was working on this application I would try and investigate the restrictions on the API itself.
- price value: The products API returns a few price values and I wasn’t sure which to use out of `variantPriceRange`. I opted for `variantPriceRange.display.max`. The ‘display’ suggested it was the correct value for displaying on the site and it came with a ‘£’ already appended. I was unsure how the max/min values related - for all items in the API, the values seemed to be the same.
- price currency: have assumed all prices are in GBP
- image alt text: image alt text seemed to be provided as an empty string by the API or not provided at all. I made the assumption that images would not be helpful to users as the important information should be included in product titles and descriptions.
- product description: I made the assumption that product information is always provided as HTML. In scenarios where it’s not, it might be more appropriate to output it into a paragraph tag for better semantics.
- back button: for the purposes of this app, I set the back button link to the homepage. However, on a real e-commerce site, you would need to find out where the user had actually come from, which may be tricky if a product can be accessed from multiple pages. Would also need to account for users visiting the page directly. Would also need to account for people visiting the page with JavaScript unavailable.

### Third party libraries used

- @testing-library/react, so React components can be rendered within the tests. Recommended by Next.js documentation.
- @testing-library/jest-dom, provides custom matchers, e.g: `toBeInTheDocument` and `toBeVisible`. Recommended by Next.js documentation.

Note: one of the downsides of using these testing libraries is that it recommends testing in one way (from the POV of what a user sees) rather than worrying about the internals of an app. This isn’t I’m used to testing apps, as it checks that content is present on a page, but not that it’s rendering in the right place/as expected. I would probably do some investigation to see if there are any other recommended approaches.

### Suggested improvements

- Add schema.org properties to relevant parts of the site, e.g [Product](https://schema.org/Product). Adding these properties allows search engines to understand what your pages are showing and can help boost search engine ranking.
- Handle scenarios where API data may be empty or missing, e.g: if a product didn’t have a special offer on.
- Add sass and JavaScript linting to standardise the way they’re written in the project.
- Error handling for API calls so that:
    - if an API fetch fails, something is still shown to the user
    - if a request takes a long time, the user has some indication that data is being fetched (rather than being shown nothing)

### Suggested accessibility improvements

- Less relevant in this example, but if the page had a header and footer I would add a skip link to allow a user to bypass things that appear on every page (e.g: navigation) to skip straight to results or product information.
- Hover and focus states on interactive elements
- Either hide the carousel from screenreader users OR use aria attributes to properly associate and label the button with the images so that a user clicking the button understands what has been updated.

### Suggested performance improvements

- Check the size of images being returned from the API. It might be appropriate to have different sizes available, e.g: smaller images for thumbnails and bigger images for product image carousel.
- Tweak API to allow pagination of results. The current solution returns all 80-90 results which we then filter down to only 20. It would be better to only return 20 in the first place.
- Lazy-loading of off-screen images on the home page

### Ideal workflow
In an ideal workflow setup, PR’s would:

- have tests and linting run automatically as a required check
- require approval from at least one other developer
- could also have a visual regression testing tool (e.g: Percy) set up to catch unintended visual changes

Tricky to test no-JavaScript behaviour locally. From my understanding, disabling JavaScript in the dev tools isn’t a realistic test, as styles don’t display which would on a production site. I’d try and dig into this further and/or have review apps set up that acted like a production site, to enable this kind of testing.

---

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy.

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.
