import Head from "next/head";
import Link from "next/link";

import config from "../config/config.js";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";

export async function getServerSideProps() {
  const response = await fetch(
    `https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=${config.API_KEY}`,
    {
      headers: {
        "User-Agent": "Chrome/51.0.2704.103"
      }
    }
  );

  const data = await response.json();

  return {
    props: {
      resultsCount: data.results,
      data: data.products.slice(0,20),
    },
  };
}

const Home = ({ resultsCount, data }) => {
  let items = data;

  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers ({resultsCount})</h1>
        <div className={styles.content}>
          {items.map((item) => (
            <Link
              key={item.productId}
              href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
              }}
            >
              <a className={styles.link}>
                <ProductListItem
                  image={item.image}
                  price={item.variantPriceRange.display.max}
                  description={item.title} />
              </a>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
