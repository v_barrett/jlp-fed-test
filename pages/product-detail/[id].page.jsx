import styles from "./product-detail.module.scss";
import ProductCarousel from "../../components/product-carousel/product-carousel";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await fetch(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id,
    {
      headers: {
        "User-Agent": "Chrome/51.0.2704.103"
      }
    }
  );
  const data = await response.json();

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  return (
    <div className={styles.productPage}>
      <div className={styles.heading}>
        <a className={styles.back} href="/" aria-label="Back">&#60;</a>
        <h1>{ data.title }</h1>
      </div>
      <div>

        <div className={styles.image}>
          <ProductCarousel images={data.media.images.urls} />
        </div>

        <div className={styles.priceWrapper}>
          <p className={styles.price}><strong>£{data.price.now}</strong></p>
          <div className={styles.offer}>{data.displaySpecialOffer}</div>
          <div className={styles.services}>{data.additionalServices.includedServices}</div>
        </div>

        <div className={styles.productInfo}>
          <h2>Product information</h2>
          <div dangerouslySetInnerHTML={{ __html: data.details.productInformation }} />
          <p>Product Code: {data.code}</p>
        </div>

        <div className={styles.attributes}>
          <h2>Product specification</h2>
          <dl>
            {data.details.features[0].attributes.map((item, index) => (
              <div className={styles.attribute} key={`${item.name}${index}`}>
                <dt>{item.name}</dt>
                <dd>{item.value}</dd>
              </div>
            ))}
          </dl>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
