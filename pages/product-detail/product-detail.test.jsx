import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import ProductDetail from './[id].page'

describe('ProductDetail', () => {
  const productDetailMock = {
    title: "Product 1",
    media: {
      images: {
        urls: [
          "/link/to/image"
        ]
      }
    },
    price: {
      now: "£500"
    },
    displaySpecialOffer: "Half price today",
    additionalServices: {
      includedServices: "Free fitting"
    },
    details: {
      productInformation: "This is a great product"
    },
    code: "123456",
    details: {
      features: [
        {
          attributes: [
            {
              name: 'Color',
              value: 'red'
            }
          ]
        }
      ]
    }
  }

  it('renders product title', () => {
    render(<ProductDetail data={productDetailMock} />)

    const heading = screen.getByRole('heading', { level: 1 })

    expect(heading).toHaveTextContent('Product 1')
  })

  it('renders any special offers', () => {
    render(<ProductDetail data={productDetailMock} />)

    const offer = screen.queryByText('Half price today')

    expect(offer).toBeInTheDocument()
  })

  it('renders included services', () => {
    render(<ProductDetail data={productDetailMock} />)

    const services = screen.queryByText('Free fitting')

    expect(services).toBeInTheDocument()
  })

  it('renders product image', () => {
    render(<ProductDetail data={productDetailMock} />)

    const image = screen.getByRole('img')

    expect(image.src).toContain('/link/to/image')
  })

  it('renders product code', () => {
    render(<ProductDetail data={productDetailMock} />)

    const code = screen.queryByText('Product Code: 123456')

    expect(code).toBeInTheDocument()
  })

  it('renders a list of attributes', () => {
    const { container } = render(<ProductDetail data={productDetailMock} />)

    const attributeTitle = container.querySelector('dt')
    const attributeValue = container.querySelector('dd')

    expect(attributeTitle).toHaveTextContent('Color')
    expect(attributeValue).toHaveTextContent('red')
  })
})
