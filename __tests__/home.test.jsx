import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import Home from '../pages/index.page'

describe('Home', () => {
  const resultsCount = 25
  const productDataMock = [
    {
      productId: "123",
      title: "Product 1",
      image: "/link/to/image",
      variantPriceRange: {
        display: {
          max: "£500"
        }
      }
    },
    {
      productId: "987",
      title: "Product 2",
      image: "/link/to/image-2",
      variantPriceRange: {
        display: {
          max: "£1"
        }
      }
    }
  ]

  it('renders a heading', () => {
    render(<Home data={productDataMock} resultsCount={resultsCount} />)

    const heading = screen.getByRole('heading')

    expect(heading).toHaveTextContent('Dishwashers (25)')
  })

  it('renders a set of products', () => {
    const { container } = render(<Home data={productDataMock} resultsCount={resultsCount} />)

    const products = container.querySelectorAll('div a')

    expect(products.length).toBe(2)
  })

  it('renders a product name', () => {
    render(<Home data={productDataMock} resultsCount={resultsCount} />)

    const name = screen.queryByText('Product 2')

    expect(name).toBeInTheDocument()
  })

  it('renders a product price', () => {
    render(<Home data={productDataMock} resultsCount={resultsCount} />)

    const price = screen.queryByText('£1')

    expect(price).toBeInTheDocument()
  })
})
