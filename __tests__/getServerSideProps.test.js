import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'

import { getServerSideProps } from '../pages/index.page'

describe('Home.getServerSideProps', () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve({
          results: 22,
          products: [
            {
              productId: "123",
              title: "Product 2",
              image: "/link/to/image",
              variantPriceRange: {
                display: {
                  max: "£500"
                }
              }
            },
            {
              productId: "987",
              title: "Product 1",
              image: "/link/to/image-2",
              variantPriceRange: {
                display: {
                  max: "£1"
                }
              }
            }
          ]
        })
    })
  );

  it('returns a props object', async () => {
    const response = await getServerSideProps()

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(response.props).toEqual({
      resultsCount: 22,
      data: [
        {
          productId: "123",
          title: "Product 2",
          image: "/link/to/image",
          variantPriceRange: {
            display: {
              max: "£500"
            }
          }
        },
        {
          productId: "987",
          title: "Product 1",
          image: "/link/to/image-2",
          variantPriceRange: {
            display: {
              max: "£1"
            }
          }
        }
      ]
    })
  })
})
